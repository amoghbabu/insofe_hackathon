# INSOFE_HACKATHON

A Hackathon is Hosted by International School Engineering, Where the Problem Statement is given by the data scientist and we need to solve it individually and show our insights

## Problem Statement
<b>Predicting the sales of products across stores of a retail chain</b> <br>
A large Indian retail chain has stores across 3 states in India: Maharashtra, Telangana and Kerala. These stores stock products across various categories such as FMCG (fast moving consumer goods), eatables / perishables and others. Managing the inventory is crucial for the revenue stream of the retail chain. Meeting the demand is important to not lose potential revenue, while at the same time stocking excessive products could lead to losses.

In this hackathon you are tasked with building a machine learning model to predict the sales of products across stores for one month. These models can then be used to power the recommendations for the inventory management software at these stores.